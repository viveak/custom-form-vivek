import React, { Component } from "react";


const emailRegex = RegExp(
	/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

const formValid = ({ formErrors, ...rest }) => {
	let valid = true;

	// validate form errors being empty
	Object.values(formErrors).forEach((val) => {
		val.length > 0 && (valid = false);
	});

	// validate the form was filled out
	Object.values(rest).forEach((val) => {
		val === null && (valid = false);
	});

	return valid;
};

class Form extends Component {
	constructor(props) {
		super(props);
		this.state = {
			firstName: null,
			lastName: null,
			email: null,
			password: null,
			formErrors: {
				firstName: "",
				lastName: "",
				email: "",
				password: "",
			},
		};
	}

	handleSubmit = (event) => {
		event.preventDefault();

		if (formValid(this.state)) {
			console.log(`
        First Name: ${this.state.firstName}
        Last Name: ${this.state.lastName}
        Email: ${this.state.email}
        Password: ${this.state.password}
    `);
		} else {
			console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
		}
	};

	handleChange = (event) => {
		event.preventDefault();
		const { name, value } = event.target;
		let formErrors = { ...this.state.formErrors };

		switch (name) {
			case "firstName":
				formErrors.firstName =
					value.length < 3 ? "minimum 3 character required" : "";
				break;
			case "lastName":
				formErrors.lastName =
					value.length < 3 ? "minimum 3 character required" : "";
				break;
			case "email":
				formErrors.email = emailRegex.test(value)
					? ""
					: "invalid email address";
				break;
			case "password":
				formErrors.password =
					value.length < 6 ? "minimum 6 character required" : "";
				break;
			default:
				break;
		}

		this.setState({ 
			formErrors, 
			[name]: value
		}
		
		,
		
		() => {

			console.log(this.state)
		
		});
	};

	render() {
		const { formErrors } = this.state;

		return (
			<div className="wrapper">
				<div className="form-wrapper">
					<h1>Create Account</h1>
					<form onSubmit={this.handleSubmit} noValidate>


						<div className="firstName">
							<label htmlFor="firstName">First Name</label>
							<input
								className={formErrors.firstName.length > 0 ? "error" : null}
								placeholder="First Name"
								type="text"
								name="firstName"
								noValidate
								onChange={this.handleChange}
							/>
							{formErrors.firstName.length > 0 && (
								<span className="errorMessage">{formErrors.firstName}</span>
							)}
						</div>
						<div className="lastName">
							<label htmlFor="lastName">Last Name</label>
							<input
								className={formErrors.lastName.length > 0 ? "error" : null}
								placeholder="Last Name"
								type="text"
								name="lastName"
								noValidate
								onChange={this.handleChange}
							/>
							{formErrors.lastName.length > 0 && (
								<span className="errorMessage">{formErrors.lastName}</span>
							)}
						</div>
						<div className="email">
							<label htmlFor="email">Email</label>
							<input
								className={formErrors.email.length > 0 ? "error" : null}
								placeholder="Email"
								type="email"
								name="email"
								noValidate
								onChange={this.handleChange}
							/>
							{formErrors.email.length > 0 && (
								<span className="errorMessage">{formErrors.email}</span>
							)}
						</div>
						<div className="password">
							<label htmlFor="password">Password</label>
							<input
								className={formErrors.password.length > 0 ? "error" : null}
								placeholder="Password"
								type="password"
								name="password"
								noValidate
								onChange={this.handleChange}
							/>
							{formErrors.password.length > 0 && (
								<span className="errorMessage">{formErrors.password}</span>
							)}
						</div>
						<div style={{display:"flex",flexDirection:"column"}}>
							<div style={{margin:"1em",marginLeft:"0px"}} >
							<label htmlFor="radio">Gender :</label>
								<input type="radio" id="male" name="gender" value="male"/>
									<label htmlFor="male">Male</label>
									<input type="radio" id="female" name="gender" value="female"/>
									<label htmlFor="female">Female</label>
									<input type="radio" id="other" name="gender" value="other"/>
									<label htmlFor="other">Other</label>
							</div>
								
							<div style={{margin:"1em",marginLeft:"0px"}}>
								<input type="checkbox" id="subscribe" name="subscribe" value="newsletter"/>
								<label htmlFor="subscribe"> Subscribe to newsletter </label>
							</div>
							</div>
							<div className="email">
							<label htmlFor="address">Address : </label>
							<textarea id="address" name="address" rows="4" cols="">
							</textarea>
							</div>
							<div >
							<label htmlFor="city">Choose City:</label>

								<select name="city" id="city">uru
								<option value="Delhi">Delhi</option>
								<option value="Mumbai">Mumbai</option>
								<option value="Chennai">Chennai</option>
								<option value="Bengaluru">Bengaluru</option>
								</select>
							</div>
						<div className="createAccount">
							<button type="submit" >Create Account</button>
							<small>Already Have an Account?</small>
						</div>
						
					</form>
				</div>
			</div>
		);
	}
}

export default Form;
