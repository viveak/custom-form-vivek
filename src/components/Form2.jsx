
import React, { Component } from 'react'
import InputTypeButton from './InputTypeButton'

export class Form2 extends Component {
    constructor(props){
        super(props);
        this.state={
            text:false,
            password:false,
        }
    }

    render() {
        return (
            <div className="wrapper">
            <div className="buttonType">
            <InputTypeButton  name="text"></InputTypeButton>
			<InputTypeButton  name="password"></InputTypeButton>
			<InputTypeButton name="email"></InputTypeButton>
			<InputTypeButton name="radio"></InputTypeButton>
			<InputTypeButton name="checkbox"></InputTypeButton>
			<InputTypeButton name="currency"></InputTypeButton>
			<InputTypeButton name="Text Area"></InputTypeButton>
			<InputTypeButton name="Drop Down"></InputTypeButton></div>
			
				<div className="form-wrapper">

                <h1>Create Form</h1>
                <div className="createAccount">
				<button type="submit">Save</button>
						
						</div>
                </div>
            </div>
        )
    }
}

export default Form2
